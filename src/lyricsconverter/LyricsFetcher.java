package lyricsconverter;

import java.io.StringReader;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class LyricsFetcher {

	
	public String fetch(String artist, String songName){
		try {
			 
			Client client = Client.create();
	 
			WebResource webResource = client
			   .resource("http://api.chartlyrics.com/apiv1.asmx/SearchLyricDirect");
	 
			ClientResponse response = webResource
						.queryParam("artist", artist)
						.queryParam("song", songName)
						.get(ClientResponse.class);
	 
			if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
			}
	 
			String output = response.getEntity(String.class);
	 
			//System.out.println("Output from Server .... \n");
			//System.out.println(output);
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(output));
			Document doc = dBuilder.parse(is);
			
			NodeList nodes = doc.getElementsByTagName("Lyric");
			
			Element lyrics = (Element) nodes.item(0);
			//System.out.println(lyrics.getTextContent());
			
			return lyrics.getTextContent();
					
	 
		  } catch (Exception e) {
	 
			e.printStackTrace();
	 
		  }
		
		return null;
	}
}
