package lyricsconverter;

import java.util.ArrayList;
import java.util.List;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.SearchParameters;

public class FlickrFetcher {

	private String apikey = "d07c5a598d057fd46b23c750ce18355d";
	private String secret = "788af6054335af93 ";
	
	public List<String> run (List<String> entities){
		List<String> result = new ArrayList<String>();
		Flickr flickr = new Flickr(apikey, secret, new REST());
		
		for(String entity : entities){
			SearchParameters searchParameters = new SearchParameters();
			searchParameters.setText(entity);
			searchParameters.setSort(SearchParameters.RELEVANCE);
			//HashSet<String> extras = new HashSet<String>();
			//extras.add("description");
			//extras.add("tags");
			//searchParameters.setExtras(extras);
			
			
			PhotoList<Photo> list = null;
			try {
				searchParameters.setMedia("photos");
				list = flickr.getPhotosInterface().search(searchParameters, 0, 0);
			} catch (FlickrException e) {
				e.printStackTrace();
			}
			
			//if(list != null) System.out.println("Size "+list.getTotal());
			result.add(list.get(0).getUrl());
		}
		
		return result;
	}
}
