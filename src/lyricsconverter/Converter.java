package lyricsconverter;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
 
@Path("/lycon")
public class Converter {

	public static void main(String[] args) {
		
		int maxEntities = 5;
		
		LyricsFetcher fetcher = new LyricsFetcher();
		String text = fetcher.fetch("metallica", "whiskey in the jar");
		
		GateClient gate = new GateClient();
		List<String> entities = gate.run(text, maxEntities);
		System.out.println("Entities:"+entities);
		
		FlickrFetcher flickrFetch = new FlickrFetcher();
		List<String> urls = flickrFetch.run(entities);
		System.out.println(urls);
		
		
	}
	

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt(@QueryParam("artist") String artist, @QueryParam("song")String song) {
        //return "Got it! "+artist+song;
    	int maxEntities = 5;
		
		LyricsFetcher fetcher = new LyricsFetcher();
		String text = fetcher.fetch("metallica", "whiskey in the jar");
		
		GateClient gate = new GateClient();
		List<String> entities = gate.run(text, maxEntities);
		System.out.println("Entities:"+entities);
		/*List<String> entities = new ArrayList<String>();
		entities.add("money");*/
		
		FlickrFetcher flickrFetch = new FlickrFetcher();
		List<String> urls = flickrFetch.run(entities);
		System.out.println(urls);
		
		return urls.toString();
    }
	
}
