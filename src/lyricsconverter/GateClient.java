package lyricsconverter;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import gate.util.InvalidOffsetException;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GateClient {
	 // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;
    
    // whether the GATE is initialised
    private static boolean isGateInitilised = false;
    
    public List<String> run(String text, int max){
    	List<String> result = new ArrayList<String>();
        
        if(!isGateInitilised){
            
            // initialise GATE
            initialiseGate();            
        }        

        try {                
            // create an instance of a Document Reset processing resource
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");

            // create an instance of a English Tokeniser processing resource
            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");

            // create an instance of a Sentence Splitter processing resource
            ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            
            ProcessingResource annieGazetteer = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");
            
            ProcessingResource posTagger = (ProcessingResource) Factory.createResource("gate.creole.POSTagger");
            
            // locate the JAPE grammar file
            File japeOrigFile = new File("/home/filip/Desktop/ddw/jape_task1.jape");
            java.net.URI japeURI = japeOrigFile.toURI();
            
            // create feature map for the transducer
            FeatureMap transducerFeatureMap = Factory.newFeatureMap();
            try {
                // set the grammar location
                transducerFeatureMap.put("grammarURL", japeURI.toURL());
                // set the grammar encoding
                transducerFeatureMap.put("encoding", "UTF-8");
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL of JAPE grammar");
                System.out.println(e.toString());
            }
            
            // create an instance of a JAPE Transducer processing resource
            ProcessingResource japeTransducerPR = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);

            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(tokenizerPR);
            annotationPipeline.add(sentenceSplitterPR);
            annotationPipeline.add(annieGazetteer);
            annotationPipeline.add(posTagger);
            annotationPipeline.add(japeTransducerPR);
            
            // create a document
            //Document document = Factory.newDocument("This is some text Japan Google");
            Document document = Factory.newDocument(text);

            // create a corpus and add the document
            Corpus corpus = Factory.newCorpus("");
            corpus.add(document);

            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);

            //run the pipeline
            annotationPipeline.execute();

            // loop through the documents in the corpus
            for(int i=0; i< corpus.size(); i++){

                Document doc = corpus.get(i);

                // get the default annotation set
                AnnotationSet as_default = doc.getAnnotations();
                
                Map<String, Integer> nn = this.getStringsFromCategory("NN", as_default, doc);
                Map<String, Integer> jj_nn = this.getStringsFromCategory("JJ_NN", as_default, doc);
                
                System.out.println("jj_nn:"+jj_nn);
                System.out.println("nn:"+nn);
                
                
                List<String> top_jj_nn = this.takeTop(jj_nn, max);
                System.out.println("top_jj_nn:"+top_jj_nn);
                result.addAll(top_jj_nn);
                
                if(top_jj_nn.size() < max){
	                List<String> top_nn = this.takeTop(nn, max-top_jj_nn.size());
	                System.out.println("top_nn:"+top_nn);
	                result.addAll(top_nn);
                }
                
                return result;

            }
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        
        return null;
    }

    private Map<String, Integer> getStringsFromCategory(String category, AnnotationSet as_default, Document doc) throws InvalidOffsetException{
    	FeatureMap futureMap = null;
    	AnnotationSet annSetTokens = as_default.get(category, futureMap);
    	//System.out.println("Category: "+category);
        //System.out.println("Number of Token annotations: " + annSetTokens.size());
        
        Map<String, Integer> result = new HashMap<String, Integer>();

        ArrayList tokenAnnotations = new ArrayList(annSetTokens);

        // looop through the Token annotations
        for(int j = 0; j < tokenAnnotations.size(); ++j) {

            // get a token annotation
            Annotation token = (Annotation)tokenAnnotations.get(j);

            // get the underlying string for the Token
            Node isaStart = token.getStartNode();
            Node isaEnd = token.getEndNode();
            String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
            //System.out.println("\tToken: " + underlyingString);
            
            if(result.containsKey(underlyingString)){
            	result.put(underlyingString, result.get(underlyingString) + 1);
            }else{
            	result.put(underlyingString, 1);
            }
            
        }
        
        return result;
    }
    
    
    @SuppressWarnings("unchecked")
	private List<String> takeTop(Map<String, Integer> map, int count){
    	List<String> result = new ArrayList<String>();
    	Object[] a = map.entrySet().toArray();
        Arrays.sort(a, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Map.Entry<String, Integer>) o2).getValue().compareTo(
                        ((Map.Entry<String, Integer>) o1).getValue());
            }
        });
        for (int i = 0; i < count; i++) {
        	if(i >= a.length)break;
        	
            /*System.out.println(((Map.Entry<String, Integer>) a[i]).getKey() + " : "
                    + ((Map.Entry<String, Integer>) a[i]).getValue());*/
            
            result.add(((Map.Entry<String, Integer>) a[i]).getKey());
    	
        }
    	return result;
    }
    
    private void initialiseGate() {
        
        try {
            // set GATE home folder
            // Eg. /Applications/GATE_Developer_7.0
            File gateHomeFile = new File("/home/filip/Development/GATE_Developer_8.0");
            Gate.setGateHome(gateHomeFile);
            
            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.0/plugins            
            File pluginsHome = new File("/home/filip/Development/GATE_Developer_8.0/plugins");
            Gate.setPluginsHome(pluginsHome);            
            
            // set user config file (optional)
            // Eg. /Applications/GATE_Developer_7.0/user.xml
            Gate.setUserConfigFile(new File("/home/filip/Development/GATE_Developer_8.0", "user.xml"));            
            
            // initialise the GATE library
            Gate.init();
            
            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);
            
            // flag that GATE was successfuly initialised
            isGateInitilised = true;
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}